.PHONY: help install encore tests tests-ci stan

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: ## Install vendors
	composer install
	yarn install
	php bin/console do:da:cr
	php bin/console do:sc:up --force

encore: ## Assets compilations
	yarn encore dev

tests: ## Run tests
	./bin/phpunit --coverage-html=var/coverage --coverage-clover=coverage.xml

tests-ci: ## Launch PHPUnit tests for CI
	./bin/phpunit --coverage-text --colors=never

tests-docker: ## Launch tests in Docker test container
	docker run -v ${PWD}:/app:cached martindamien/bierkeller:test

stan: ## Run PHPStan
	./vendor/bin/phpstan analyse -l max src
