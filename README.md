# Bierkeller

**🚨 This is a work in progress app and is absolutely not ready for production. 🚨**

## Mount external database

For production, you'll need to mount a folder to keep database through container reboots.

If you need to mount an external database, you can mount the containing folder as `app/var/database`.

## Running tests

You can run tests inside the `martindamien/bierkeller:dev` image by running `make tests`
but it will not generate the coverage.

You can also run tests inside a dedicated container (`martindamien/bierkeller:dev`) by running
`make tests-docker` on the host machine. (**This is a very slow way to run tests**)
