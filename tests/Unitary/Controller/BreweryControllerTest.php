<?php

namespace App\Tests\Unitary\Controller;

use App\Controller\BreweryController;
use App\Repository\BeerRepository;
use App\Repository\BreweryRepository;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\Messenger\MessageBusInterface;

final class BreweryControllerTest extends TestCase
{
    private $messageBus;
    private $beerRepository;
    private $breweryRepository;
    private $breweryController;

    protected function setUp()
    {
        $this->messageBus = $this->prophesize(MessageBusInterface::class);
        $this->beerRepository = $this->prophesize(BeerRepository::class);
        $this->breweryRepository = $this->prophesize(BreweryRepository::class);

        $this->breweryController = new BreweryController(
            $this->messageBus->reveal(),
            $this->breweryRepository->reveal(),
            $this->beerRepository->reveal()
        );
    }

    public function testItCanBeInitialized(): void
    {
        $this->assertInstanceOf(BreweryController::class, $this->breweryController);
    }
}
