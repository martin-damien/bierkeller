<?php

namespace App\Tests\Unitary\Controller;

use App\Controller\BeerController;
use App\Repository\BeerRepository;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\Messenger\MessageBusInterface;

final class BeerControllerTest extends TestCase
{
    private $messageBus;
    private $beerRepository;
    private $beerController;

    protected function setUp()
    {
        $this->messageBus = $this->prophesize(MessageBusInterface::class);
        $this->beerRepository = $this->prophesize(BeerRepository::class);

        $this->beerController = new BeerController(
            $this->messageBus->reveal(),
            $this->beerRepository->reveal()
        );
    }

    public function testItCanBeInitialized(): void
    {
        $this->assertInstanceOf(BeerController::class, $this->beerController);
    }
}
