<?php

namespace App\Tests\Unitary\Event;

use App\Entity\Brewery;
use App\Event\BreweryAddedEvent;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class BreweryAddedEventTest extends TestCase
{
    private $brewery;
    private $breweryAddedEvent;

    protected function setUp()
    {
        $this->brewery = new Brewery('My little brewery');
        $this->breweryAddedEvent = new BreweryAddedEvent($this->brewery);
    }

    public function testItCanBeInitialized()
    {
        $this->assertInstanceOf(BreweryAddedEvent::class, $this->breweryAddedEvent);
    }

    public function testItCanGetBrewery()
    {
        $this->assertEquals($this->brewery, $this->breweryAddedEvent->getBrewery());
    }
}