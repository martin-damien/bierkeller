<?php

namespace App\Tests\Unitary\Event;

use App\Entity\Brewery;
use App\Event\BreweryEditedEvent;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class BreweryEditedEventTest extends TestCase
{
    private $brewery;
    private $breweryEditedEvent;

    protected function setUp()
    {
        $this->brewery = new Brewery('My little brewery');
        $this->breweryEditedEvent = new BreweryEditedEvent($this->brewery);
    }

    public function testItCanBeInitialized()
    {
        $this->assertInstanceOf(BreweryEditedEvent::class, $this->breweryEditedEvent);
    }

    public function testItCanGetBrewery()
    {
        $this->assertEquals($this->brewery, $this->breweryEditedEvent->getBrewery());
    }
}
