<?php

namespace App\Tests\Unitary\Event;

use App\Entity\Beer;
use App\Event\BeerEditedEvent;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class BeerEditedEventTest extends TestCase
{
    private $beer;
    private $beerEditedEvent;

    protected function setUp()
    {
        $this->beer = new Beer('My little brewery');
        $this->beerEditedEvent = new BeerEditedEvent($this->beer);
    }

    public function testItCanBeInitialized()
    {
        $this->assertInstanceOf(BeerEditedEvent::class, $this->beerEditedEvent);
    }

    public function testItCanGetBeer()
    {
        $this->assertEquals($this->beer, $this->beerEditedEvent->getBeer());
    }
}
