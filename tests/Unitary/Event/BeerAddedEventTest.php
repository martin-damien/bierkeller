<?php

namespace App\Tests\Unitary\Event;

use App\Entity\Beer;
use App\Event\BeerAddedEvent;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class BeerAddedEventTest extends TestCase
{
    private $beer;
    private $beerAddedEvent;

    protected function setUp()
    {
        $this->beer = new Beer('My little brewery');
        $this->beerAddedEvent = new BeerAddedEvent($this->beer);
    }

    public function testItCanBeInitialized()
    {
        $this->assertInstanceOf(BeerAddedEvent::class, $this->beerAddedEvent);
    }

    public function testItCanGetBeer()
    {
        $this->assertEquals($this->beer, $this->beerAddedEvent->getBeer());
    }
}
