<?php

namespace App\Tests\Unitary\Entity;

use App\Entity\Beer;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class BeerTest extends TestCase
{
    private $beer;

    protected function setUp()
    {
        $this->beer = new Beer('Incredible Stout');

        $reflectionClass = new \ReflectionClass($this->beer);
        $property = $reflectionClass->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($this->beer, Uuid::fromString('6a276650-abb6-432b-9a33-9be0f67f06c5'));
    }

    public function testItCanBeInitialized(): void
    {
        $this->assertInstanceOf(Beer::class, $this->beer);
    }

    public function testIdIsAccessible(): void
    {
        $this->assertEquals('6a276650-abb6-432b-9a33-9be0f67f06c5', $this->beer->getId());
    }

    public function testItRendersAsString(): void
    {
        $this->assertEquals('Incredible Stout', $this->beer->__toString());
    }
}
