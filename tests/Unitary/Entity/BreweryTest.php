<?php

namespace App\Tests\Unitary\Entity;

use App\Entity\Brewery;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class BreweryTest extends TestCase
{
    private $brewery;

    protected function setUp()
    {
        $this->brewery = new Brewery('My little brewery');

        $reflectionClass = new \ReflectionClass($this->brewery);
        $property = $reflectionClass->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($this->brewery, Uuid::fromString('6a276650-abb6-432b-9a33-9be0f67f06c5'));
    }

    public function testItCanBeInitialized(): void
    {
        $this->assertInstanceOf(Brewery::class, $this->brewery);
    }

    public function testIdIsAccessible(): void
    {
        $this->assertEquals('6a276650-abb6-432b-9a33-9be0f67f06c5', $this->brewery->getId());
    }

    public function testItRendersAsString(): void
    {
        $this->assertEquals('My little brewery', $this->brewery->__toString());
    }
}
