<?php

namespace App\Tests\Unitary\Handler;

use App\Command\EditBeerCommand;
use App\Entity\Beer;
use App\Event\BeerEditedEvent;
use App\Event\BeerEvents;
use App\Handler\EditBeerCommandHandler;
use App\Repository\BeerRepository;
use Prophecy\Argument;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class EditBeerCommandHandlerTest extends TestCase
{
    private $eventDispatcher;
    private $beerRepository;
    private $editBeerCommandHandler;

    protected function setUp()
    {
        $this->eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
        $this->beerRepository = $this->prophesize(BeerRepository::class);

        $this->editBeerCommandHandler = new EditBeerCommandHandler(
            $this->eventDispatcher->reveal(),
            $this->beerRepository->reveal()
        );
    }

    public function testItCanBeInitialized(): void
    {
        $this->assertInstanceOf(EditBeerCommandHandler::class, $this->editBeerCommandHandler);
    }

    public function testItSavesBeer(): void
    {
        $beer = new Beer('Awesome IPA');
        $command = EditBeerCommand::fromBeer($beer);
        $command->name = 'Awesome Lager';

        $this->beerRepository->save(
            Argument::allOf(
                Argument::type(Beer::class),
                Argument::which('getName', 'Awesome Lager')
            )
        )->shouldBeCalled();

        $this->eventDispatcher->dispatch(BeerEvents::EDITED, Argument::type(BeerEditedEvent::class))->shouldBeCalled();

        $this->editBeerCommandHandler->__invoke($command);
    }
}
