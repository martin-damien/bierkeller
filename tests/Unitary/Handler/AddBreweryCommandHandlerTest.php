<?php

namespace App\Tests\Unitary\Handler;

use App\Command\AddBreweryCommand;
use App\Entity\Brewery;
use App\Event\BreweryAddedEvent;
use App\Event\BreweryEvents;
use App\Handler\AddBreweryCommandHandler;
use App\Repository\BreweryRepository;
use Prophecy\Argument;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class AddBreweryCommandHandlerTest extends TestCase
{
    private $eventDispatcher;
    private $breweryRepository;
    private $addBreweryCommandHandler;

    protected function setUp()
    {
        $this->eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
        $this->breweryRepository = $this->prophesize(BreweryRepository::class);

        $this->addBreweryCommandHandler = new AddBreweryCommandHandler(
            $this->eventDispatcher->reveal(),
            $this->breweryRepository->reveal()
        );
    }

    public function testItCanBeInitialized()
    {
        $this->assertInstanceOf(AddBreweryCommandHandler::class, $this->addBreweryCommandHandler);
    }

    public function testItSavesBrewery()
    {
        $command = new AddBreweryCommand();
        $command->name = 'My little brewery';
        $command->country = 'UK';

        $this->breweryRepository->save(
            Argument::allOf(
                Argument::type(Brewery::class),
                Argument::which('getName', 'My little brewery'),
                Argument::which('getCountry', 'UK')
            )
        )->shouldBeCalled();

        $this->eventDispatcher->dispatch(BreweryEvents::ADDED, Argument::type(BreweryAddedEvent::class))->shouldBeCalled();

        $this->addBreweryCommandHandler->__invoke($command);
    }
}