<?php

namespace App\Tests\Unitary\Handler;

use App\Command\AddBeerCommand;
use App\Entity\Beer;
use App\Event\BeerAddedEvent;
use App\Event\BeerEvents;
use App\Handler\AddBeerCommandHandler;
use App\Repository\BeerRepository;
use Prophecy\Argument;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class AddBeerCommandHandlerTest extends TestCase
{
    private $eventDispatcher;
    private $beerRepository;
    private $addBeerCommandHandler;

    protected function setUp()
    {
        $this->eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
        $this->beerRepository = $this->prophesize(BeerRepository::class);

        $this->addBeerCommandHandler = new AddBeerCommandHandler(
            $this->eventDispatcher->reveal(),
            $this->beerRepository->reveal()
        );
    }

    public function testItCanBeInitialized(): void
    {
        $this->assertInstanceOf(AddBeerCommandHandler::class, $this->addBeerCommandHandler);
    }

    public function testItSavesBeer(): void
    {
        $command = new AddBeerCommand();
        $command->name = 'Awesome IPA';

        $this->beerRepository->save(
            Argument::allOf(
                Argument::type(Beer::class),
                Argument::which('getName', 'Awesome IPA')
            )
        )->shouldBeCalled();

        $this->eventDispatcher->dispatch(BeerEvents::ADDED, Argument::type(BeerAddedEvent::class))->shouldBeCalled();

        $this->addBeerCommandHandler->__invoke($command);
    }
}
