<?php

namespace App\Tests\Unitary\Handler;

use App\Command\EditBreweryCommand;
use App\Entity\Brewery;
use App\Event\BreweryEditedEvent;
use App\Event\BreweryEvents;
use App\Handler\EditBreweryCommandHandler;
use App\Repository\BreweryRepository;
use Prophecy\Argument;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class EditBreweryCommandHandlerTest extends TestCase
{
    private $eventDispatcher;
    private $breweryRepository;
    private $editBreweryCommandHandler;

    protected function setUp()
    {
        $this->eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
        $this->breweryRepository = $this->prophesize(BreweryRepository::class);

        $this->editBreweryCommandHandler = new EditBreweryCommandHandler(
            $this->eventDispatcher->reveal(),
            $this->breweryRepository->reveal()
        );
    }

    public function testItCanBeInitialized()
    {
        $this->assertInstanceOf(EditBreweryCommandHandler::class, $this->editBreweryCommandHandler);
    }

    public function testItSavesBrewery()
    {
        $brewery = new Brewery('My little brewery');
        $brewery->setCountry('BE');

        $command = EditBreweryCommand::fromBrewery($brewery);
        $command->name = 'My awesome brewery';
        $command->country = 'UK';

        $this->breweryRepository->save(
            Argument::allOf(
                Argument::type(Brewery::class),
                Argument::which('getName', 'My awesome brewery'),
                Argument::which('getCountry', 'UK')
            )
        )->shouldBeCalled();

        $this->eventDispatcher->dispatch(BreweryEvents::EDITED, Argument::type(BreweryEditedEvent::class))->shouldBeCalled();

        $this->editBreweryCommandHandler->__invoke($command);
    }
}
