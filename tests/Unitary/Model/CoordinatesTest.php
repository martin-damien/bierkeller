<?php

namespace App\Tests\Unitary\Model;

use App\Model\Coordinates;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class CoordinatesTest extends TestCase
{
    private $coordinates;

    protected function setUp()
    {
        $this->coordinates = new Coordinates(42.5, 4.8);
    }

    public function testItIsCorrectlyCreated()
    {
        $this->assertInstanceOf(Coordinates::class, $this->coordinates);
    }

    public function testItReturnsLatitude()
    {
        $this->assertEquals(42.5, $this->coordinates->getLatitude());
    }

    public function testItReturnsLongitude()
    {
        $this->assertEquals(4.8, $this->coordinates->getLongitude());
    }
}