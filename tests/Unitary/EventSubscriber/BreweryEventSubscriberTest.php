<?php

namespace App\Tests\Unitary\EventSubscriber;

use App\Entity\Brewery;
use App\Event\BreweryAddedEvent;
use App\Event\BreweryEditedEvent;
use App\Event\BreweryEvents;
use App\EventSubscriber\BreweryEventSubscriber;
use Prophecy\Argument;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

final class BreweryEventSubscriberTest extends TestCase
{
    private $flashBag;
    private $breweryEventSubscriber;

    protected function setUp()
    {
        $this->flashBag = $this->prophesize(FlashBagInterface::class);

        $this->breweryEventSubscriber = new BreweryEventSubscriber($this->flashBag->reveal());
    }

    public function testItCanBeInitialized(): void
    {
        $this->assertInstanceOf(BreweryEventSubscriber::class, $this->breweryEventSubscriber);
    }

    public function testSubscribedEvents(): void
    {
        $this->assertArrayHasKey(BreweryEvents::ADDED, $this->breweryEventSubscriber::getSubscribedEvents());
        $this->assertArrayHasKey(BreweryEvents::EDITED, $this->breweryEventSubscriber::getSubscribedEvents());
    }

    public function testItNotifyWhenAdded(): void
    {
        $brewery = new Brewery('My little brewery');
        $event = new BreweryAddedEvent($brewery);

        $this->flashBag->add('success', Argument::allOf(
            Argument::type('string'),
            Argument::containingString('My little brewery')
        ))->shouldBeCalled();

        $this->breweryEventSubscriber->notifyWhenAdded($event);
    }

    public function testItNotifyWhenEdited(): void
    {
        $brewery = new Brewery('My little brewery');
        $event = new BreweryEditedEvent($brewery);

        $this->flashBag->add('success', Argument::allOf(
            Argument::type('string'),
            Argument::containingString('My little brewery')
        ))->shouldBeCalled();

        $this->breweryEventSubscriber->notifyWhenEdited($event);
    }
}
