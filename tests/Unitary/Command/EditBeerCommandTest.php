<?php

namespace App\Tests\Unitary\Command;

use App\Command\EditBeerCommand;
use App\Entity\Beer;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class EditBeerCommandTest extends TestCase
{
    private $beer;
    private $editBeerCommand;

    protected function setUp()
    {
        $this->beer = new Beer('Awesome IPA');
        $this->editBeerCommand = EditBeerCommand::fromBeer($this->beer);
    }

    public function testItCanBeInitialized()
    {
        $this->assertInstanceOf(EditBeerCommand::class, $this->editBeerCommand);
    }

    public function testItCanGetBeer()
    {
        $this->assertEquals($this->beer, $this->editBeerCommand->getBeer());
    }
}
