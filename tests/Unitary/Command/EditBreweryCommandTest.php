<?php

namespace App\Tests\Unitary\Command;

use App\Command\EditBreweryCommand;
use App\Entity\Brewery;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

final class EditBreweryCommandTest extends TestCase
{
    private $brewery;
    private $editBreweryCommand;

    protected function setUp()
    {
        $this->brewery = new Brewery('My Little Brewery');
        $this->editBreweryCommand = EditBreweryCommand::fromBrewery($this->brewery);
    }

    public function testItCanBeInitialized()
    {
        $this->assertInstanceOf(EditBreweryCommand::class, $this->editBreweryCommand);
    }

    public function testItCanGetBeer()
    {
        $this->assertEquals($this->brewery, $this->editBreweryCommand->getBrewery());
    }
}
