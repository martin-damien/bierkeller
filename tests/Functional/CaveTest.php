<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

final class CaveTest extends WebTestCase
{
    private $client;

    protected function setUp()
    {
        $this->client = static::createClient();
    }

    public function testHomepage(): void
    {
        $this->client->request('GET', '/cave/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}
