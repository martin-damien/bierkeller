<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

final class BreweryTest extends WebTestCase
{
    private $client;

    protected function setUp()
    {
        $this->client = static::createClient();
    }

    public function testBreweriesListing(): void
    {
        /** @var Crawler $crawler */
        $crawler = $this->client->request('GET', '/brewery/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains('My little brewery', $crawler->text());
        $this->assertContains('My awesome brewery', $crawler->text());
    }

    public function testAddBrewery(): void
    {
        $crawler = $this->client->request('GET', '/brewery/add');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Add')->form();
        $form['add_brewery_form[name]'] = 'Lambic trader';

        $this->client->submit($form);

        $crawler = $this->client->followRedirect();

        $this->assertContains('Breweries', $crawler->text());
        $this->assertContains('Lambic trader', $crawler->text());
    }

    public function testEditBrewery(): void
    {
        // Edit "Sandbox beer" (cf. Fixtures)
        $crawler = $this->client->request('GET', sprintf('/brewery/%s/edit', 'e759de15-705c-4048-b0f1-f52e6c6c8aae'));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Save')->form();
        $form['edit_brewery_form[name]'] = 'The underground';

        $this->client->submit($form);

        $crawler = $this->client->followRedirect();

        $this->assertContains('Beers', $crawler->text());
        $this->assertContains('The underground', $crawler->text());
    }

    /** @depends testEditBrewery */
    public function testViewBrewery(): void
    {
        $crawler = $this->client->request('GET', sprintf('/brewery/%s', 'e759de15-705c-4048-b0f1-f52e6c6c8aae'));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains('The underground', $crawler->text());
    }
}
