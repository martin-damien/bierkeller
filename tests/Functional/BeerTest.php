<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

final class BeerTest extends WebTestCase
{
    private $client;

    protected function setUp()
    {
        $this->client = static::createClient();
    }

    public function testBeersListing(): void
    {
        /** @var Crawler $crawler */
        $crawler = $this->client->request('GET', '/beer/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains('Awesome Lager', $crawler->text());
        $this->assertContains('Awesome IPA', $crawler->text());
        $this->assertContains('Incredible Stout', $crawler->text());
    }

    public function testAddBeer(): void
    {
        $crawler = $this->client->request('GET', '/beer/add');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Add')->form();
        $form['add_beer_form[name]'] = 'Kriek';
        $form['add_beer_form[rate]'] = 2;
        $form['add_beer_form[alcohol]'] = 0.0;

        $this->client->submit($form);

        $crawler = $this->client->followRedirect();

        $this->assertContains('Beers', $crawler->text());
        $this->assertContains('Kriek', $crawler->text());
    }

    public function testEditBeer(): void
    {
        // Edit "Sandbox beer" (cf. Fixtures)
        $crawler = $this->client->request('GET', sprintf('/beer/%s/edit', '6a276650-abb6-432b-9a33-9be0f67f06c5'));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Save')->form();
        $form['edit_beer_form[name]'] = 'Pils';
        $form['edit_beer_form[rate]'] = 3;
        $form['edit_beer_form[alcohol]'] = 0.0;

        $this->client->submit($form);

        $crawler = $this->client->followRedirect();

        $this->assertContains('Beers', $crawler->text());
        $this->assertContains('Pils', $crawler->text());
    }

    /** @depends testEditBeer */
    public function testViewBeer(): void
    {
        $crawler = $this->client->request('GET', sprintf('/beer/%s/edit', '6a276650-abb6-432b-9a33-9be0f67f06c5'));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains('Pils', $crawler->text());
    }
}
