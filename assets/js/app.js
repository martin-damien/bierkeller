require('../css/app.scss');
require('@fortawesome/fontawesome-free/js/all.js');
require('bootstrap');

require('./beer_form');

$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

});