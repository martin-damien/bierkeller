const $ = require('jquery');

$(document).ready(function () {

    $('#quick_add_brewery_form').on('submit', function (evt) {
        evt.preventDefault();
    });

    $('#breweryQuickAdd').on('click', function (evt) {

       evt.preventDefault();

       let $select = $($('#breweryQuickAddLink').attr('data-select'));

       $.ajax('/api/breweries', {
           method: 'POST',
           contentType: 'application/json',
           dataType: 'json',
           data: JSON.stringify({
               name: $('#quick_add_brewery_form_name').val()
           })
       }).done(function (result) {

            let option = new Option(result.name, result.id);

            $(option).html(result.name);
            $select.append(option);

            $select.val(result.id);

            $('#breweryModal').modal('hide');
       });

    });

});