<?php

namespace App\Event;

final class BreweryEvents
{
    public const ADDED = 'brewery.added';
    public const EDITED = 'brewery.edited';
}
