<?php

namespace App\Event;

use App\Entity\Beer;
use Symfony\Component\EventDispatcher\Event;

final class BeerRemovedFromStockEvent extends Event
{
    private $beer;
    private $quantity;

    public function __construct(Beer $beer, int $quantity)
    {
        $this->beer = $beer;
        $this->quantity = $quantity;
    }

    public function getBeer(): Beer
    {
        return $this->beer;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
