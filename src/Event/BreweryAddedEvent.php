<?php

namespace App\Event;

use App\Entity\Brewery;
use Symfony\Component\EventDispatcher\Event;

final class BreweryAddedEvent extends Event
{
    private $brewery;

    public function __construct(Brewery $brewery)
    {
        $this->brewery = $brewery;
    }

    public function getBrewery(): Brewery
    {
        return $this->brewery;
    }
}
