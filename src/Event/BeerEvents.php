<?php

namespace App\Event;

final class BeerEvents
{
    public const ADDED = 'beer.added';
    public const EDITED = 'beer.edited';
}
