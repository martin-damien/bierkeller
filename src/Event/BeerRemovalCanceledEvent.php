<?php

namespace App\Event;

use App\Entity\Beer;
use Symfony\Component\EventDispatcher\Event;

final class BeerRemovalCanceledEvent extends Event
{
    private $beer;

    public function __construct(Beer $beer)
    {
        $this->beer = $beer;
    }

    public function getBeer(): Beer
    {
        return $this->beer;
    }
}
