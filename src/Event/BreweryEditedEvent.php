<?php

namespace App\Event;

use App\Entity\Brewery;
use Symfony\Component\EventDispatcher\Event;

final class BreweryEditedEvent extends Event
{
    private $brewery;

    public function __construct(Brewery $brewery)
    {
        $this->brewery = $brewery;
    }

    public function getBrewery(): Brewery
    {
        return $this->brewery;
    }
}
