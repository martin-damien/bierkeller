<?php

namespace App\Event;

final class StockEvents
{
    public const BEER_ADDED = 'stock.beer_added';
    public const BEER_REMOVED = 'stock.beer_removed';
    public const BEER_REMOVAL_CANCELED = 'stock.beer_removal_canceled';
}
