<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

final class EBC extends Enum
{
    private const EBC_1_4 = '1-4';
    private const EBC_4_8 = '4-8';
    private const EBC_8_20 = '8-20';
    private const EBC_20_35 = '20-35';
    private const EBC_35_60 = '35-60';
    private const EBC_60 = '60+';
}
