<?php

namespace App\Command;

use App\Entity\Beer;
use Symfony\Component\Validator\Constraints as Assert;

final class EditBeerCommand
{
    private $beer;

    /**
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @Assert\Range(min="0", max="5")
     */
    public $rate;

    public $style;

    public $brewery;

    public $alcohol;

    public $colour;

    public $bottleSize;

    public $bottleType;

    public $notes;

    private function __construct(Beer $beer)
    {
        $this->beer = $beer;
    }

    public static function fromBeer(Beer $beer): EditBeerCommand
    {
        $command = new self($beer);
        $command->name = $beer->getName();
        $command->rate = $beer->getRate();
        $command->style = $beer->getStyle();
        $command->brewery = $beer->getBrewery();
        $command->alcohol = $beer->getAlcohol();
        $command->colour = $beer->getColour();
        $command->bottleSize = $beer->getBottleSize();
        $command->bottleType = $beer->getBottleType();
        $command->notes = $beer->getNotes();

        return $command;
    }

    public function getBeer(): Beer
    {
        return $this->beer;
    }
}
