<?php

namespace App\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class AddBeerCommand
{
    /**
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @Assert\Range(min="0", max="5")
     */
    public $rate;

    public $style;

    public $brewery;

    public $alcohol;

    public $colour;

    public $bottleSize;

    public $bottleType;

    public $notes;
}
