<?php

namespace App\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class AddBreweryCommand
{
    /**
     * @Assert\NotBlank()
     */
    public $name;

    public $country;
}
