<?php

namespace App\Command;

use App\Entity\Brewery;
use Symfony\Component\Validator\Constraints as Assert;

final class EditBreweryCommand
{
    private $brewery;

    /**
     * @Assert\NotBlank()
     */
    public $name;

    public $country;

    private function __construct($brewery)
    {
        $this->brewery = $brewery;
    }

    public static function fromBrewery(Brewery $brewery): EditBreweryCommand
    {
        $command = new self($brewery);
        $command->name = $brewery->getName();
        $command->country = $brewery->getCountry();

        return $command;
    }

    public function getBrewery(): Brewery
    {
        return $this->brewery;
    }
}
