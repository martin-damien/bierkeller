<?php

namespace App\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class AddStockCommand
{
    /**
     * @Assert\NotBlank()
     */
    public $beer;

    /**
     * @Assert\GreaterThanOrEqual(0)
     */
    public $quantity;
}
