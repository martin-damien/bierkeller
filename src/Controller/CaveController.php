<?php

namespace App\Controller;

use App\Command\AddOneBeerCommand;
use App\Command\AddStockCommand;
use App\Command\RemoveOneBeerCommand;
use App\Entity\Beer;
use App\Form\Type\AddStockFormType;
use App\Repository\StockRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cave")
 */
final class CaveController extends AbstractController
{
    private $messageBus;
    private $stockRepository;

    public function __construct(MessageBusInterface $messageBus, StockRepository $stockRepository)
    {
        $this->messageBus = $messageBus;
        $this->stockRepository = $stockRepository;
    }

    /**
     * @Route("/", name="cave.index")
     */
    public function index(): Response
    {
        $stock = $this->stockRepository->findAll();

        return $this->render('cave/index.html.twig', [
            'stock' => $stock,
        ]);
    }

    /**
     * @Route("/add", name="cave.add")
     */
    public function add(Request $request): Response
    {
        $command = new AddStockCommand();
        $form = $this->createForm(AddStockFormType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->messageBus->dispatch($command);

            return $this->redirectToRoute('cave.index');
        }

        return $this->render('cave/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{beer}/add-one", name="cave.add_one")
     */
    public function addOneBeer(Beer $beer): Response
    {
        $command = new AddOneBeerCommand();
        $command->beer = $beer;

        $this->messageBus->dispatch($command);

        return $this->redirectToRoute('cave.index');
    }

    /**
     * @Route("/{beer}/remove-one", name="cave.remove_one")
     */
    public function removeOneBeer(Beer $beer): Response
    {
        $command = new RemoveOneBeerCommand();
        $command->beer = $beer;

        $this->messageBus->dispatch($command);

        return $this->redirectToRoute('cave.index');
    }
}