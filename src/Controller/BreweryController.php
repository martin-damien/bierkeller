<?php

namespace App\Controller;

use App\Command\AddBreweryCommand;
use App\Command\EditBreweryCommand;
use App\Entity\Brewery;
use App\Form\Type\AddBreweryFormType;
use App\Form\Type\EditBreweryFormType;
use App\Repository\BeerRepository;
use App\Repository\BreweryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/brewery")
 */
final class BreweryController extends AbstractController
{
    private $messageBus;
    private $breweryRepository;
    private $beerRepository;

    public function __construct(MessageBusInterface $messageBus, BreweryRepository $breweryRepository, BeerRepository $beerRepository)
    {
        $this->messageBus = $messageBus;
        $this->breweryRepository = $breweryRepository;
        $this->beerRepository = $beerRepository;
    }

    /**
     * @Route("/", name="brewery.index")
     */
    public function index(): Response
    {
        $breweries = $this->breweryRepository->findAll();

        return $this->render('brewery/index.html.twig', [
            'breweries' => $breweries,
        ]);
    }

    /**
     * @Route("/add", name="brewery.add")
     */
    public function add(Request $request): Response
    {
        $command = new AddBreweryCommand();
        $form = $this->createForm(AddBreweryFormType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->messageBus->dispatch($command);

            return $this->redirectToRoute('brewery.index');
        }

        return $this->render('brewery/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{brewery}/edit", name="brewery.edit")
     */
    public function edit(Request $request, Brewery $brewery): Response
    {
        $command = EditBreweryCommand::fromBrewery($brewery);
        $form = $this->createForm(EditBreweryFormType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->messageBus->dispatch($command);

            return $this->redirectToRoute('brewery.view', ['brewery' => $brewery->getId()]);
        }

        return $this->render('brewery/edit.html.twig', [
            'brewery' => $brewery,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{brewery}", name="brewery.view")
     */
    public function view(Brewery $brewery): Response
    {
        $beers = $this->beerRepository->findBy(['brewery' => $brewery->getId()]);

        return $this->render('brewery/view.html.twig', [
            'brewery' => $brewery,
            'beers' => $beers,
        ]);
    }
}
