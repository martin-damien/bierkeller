<?php

namespace App\Controller;

use App\Command\AddBeerCommand;
use App\Command\EditBeerCommand;
use App\Entity\Beer;
use App\Form\Type\AddBeerFormType;
use App\Form\Type\EditBeerFormType;
use App\Form\Type\QuickAddBreweryFormType;
use App\Repository\BeerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/beer")
 */
final class BeerController extends AbstractController
{
    private $messageBus;
    private $beerRepository;

    public function __construct(MessageBusInterface $messageBus, BeerRepository $beerRepository)
    {
        $this->messageBus = $messageBus;
        $this->beerRepository = $beerRepository;
    }

    /**
     * @Route("/", name="beer.index")
     */
    public function index()
    {
        $beers = $this->beerRepository->findAll();

        return $this->render('beer/index.html.twig', [
            'beers' => $beers,
        ]);
    }

    /**
     * @Route("/add", name="beer.add")
     */
    public function add(Request $request): Response
    {
        $command = new AddBeerCommand();
        $form = $this->createForm(AddBeerFormType::class, $command);
        $breweryForm = $this->createForm(QuickAddBreweryFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->messageBus->dispatch($command);

            return $this->redirectToRoute('beer.index');
        }

        return $this->render('beer/add.html.twig', [
            'form' => $form->createView(),
            'brewery_form' => $breweryForm->createView(),
        ]);
    }

    /**
     * @Route("/{beer}/edit", name="beer.edit")
     */
    public function edit(Request $request, Beer $beer): Response
    {
        $command = EditBeerCommand::fromBeer($beer);
        $form = $this->createForm(EditBeerFormType::class, $command);
        $breweryForm = $this->createForm(QuickAddBreweryFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->messageBus->dispatch($command);

            return $this->redirectToRoute('beer.view', ['beer' => $beer->getId()]);
        }

        return $this->render('beer/edit.html.twig', [
            'beer' => $beer,
            'form' => $form->createView(),
            'brewery_form' => $breweryForm->createView(),
        ]);
    }

    /**
     * @Route("/{beer}", name="beer.view")
     */
    public function view(Beer $beer)
    {
        return $this->render('beer/view.html.twig', [
            'beer' => $beer,
        ]);
    }
}
