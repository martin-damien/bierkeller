<?php

namespace App\Form\Type;

use App\Enum\EBC;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EBCType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => EBC::toArray(),
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
