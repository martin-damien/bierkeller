<?php

namespace App\Form\Type;

use App\Command\AddBeerCommand;
use App\Entity\Brewery;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class AddBeerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name', TextType::class);

        $builder->add('rate', IntegerType::class, [
            'empty_data' => 2,
            'required' => false,
            'help' => 'Must be between 0 and 5',
        ]);

        $builder->add('style', TextType::class, [
            'required' => false,
        ]);

        $builder->add('brewery', EntityType::class, [
            'class' => Brewery::class,
            'required' => false,
        ]);

        $builder->add('alcohol', PercentType::class, [
            'required' => false,
            'empty_data' => 0,
        ]);

        $builder->add('colour', EBCType::class, [
            'required' => false,
        ]);

        $builder->add('bottleSize', TextType::class, [
            'required' => false,
        ]);

        $builder->add('bottleType', TextType::class, [
            'required' => false,
        ]);

        $builder->add('notes', TextareaType::class, [
            'required' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AddBeerCommand::class,
        ]);
    }
}
