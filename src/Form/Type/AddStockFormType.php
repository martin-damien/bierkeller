<?php

namespace App\Form\Type;

use App\Command\AddBeerCommand;
use App\Command\AddStockCommand;
use App\Entity\Beer;
use App\Entity\Brewery;
use App\Entity\Stock;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class AddStockFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('beer', EntityType::class, [
            'class' => Beer::class,
        ]);

        $builder->add('quantity', IntegerType::class, [
            'empty_data' => 0,
            'help' => 'Must be greater or equal to 0',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AddStockCommand::class,
        ]);
    }
}
