<?php

namespace App\Twig\Extension;

use Symfony\Component\Intl\Intl;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class TwigCountryExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('countryName', [$this, 'countryNameFromCode'])
        ];
    }

    public function countryNameFromCode($code): string
    {
        return Intl::getRegionBundle()->getCountryName($code);
    }
}
