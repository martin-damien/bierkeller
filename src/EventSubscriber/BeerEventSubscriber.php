<?php

namespace App\EventSubscriber;

use App\Command\AddStockCommand;
use App\Event\BeerAddedEvent;
use App\Event\BeerEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class BeerEventSubscriber implements EventSubscriberInterface
{
    private $flashBag;
    private $messageBus;

    public function __construct(FlashBagInterface $flashBag, MessageBusInterface $messageBus)
    {
        $this->flashBag = $flashBag;
        $this->messageBus = $messageBus;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeerEvents::ADDED => [
                ['notifyWhenAdded', 0],
                ['createStockWhenAdded', 0],
            ],
        ];
    }

    public function notifyWhenAdded(BeerAddedEvent $event): void
    {
        $this->flashBag->add(
            'success',
            sprintf('%s was created successfully.', $event->getBeer()->getName())
        );
    }

    public function createStockWhenAdded(BeerAddedEvent $event): void
    {
        $command = new AddStockCommand();
        $command->beer = $event->getBeer();
        $command->quantity = 0;

        $this->messageBus->dispatch($command);
    }
}
