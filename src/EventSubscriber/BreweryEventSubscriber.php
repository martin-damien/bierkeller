<?php

namespace App\EventSubscriber;

use App\Event\BreweryAddedEvent;
use App\Event\BreweryEditedEvent;
use App\Event\BreweryEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

final class BreweryEventSubscriber implements EventSubscriberInterface
{
    private $flashBag;

    public function __construct(FlashBagInterface $flashBag)
    {
        $this->flashBag = $flashBag;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BreweryEvents::ADDED => [
                ['notifyWhenAdded', 0],
            ],
            BreweryEvents::EDITED => [
                ['notifyWhenEdited', 0],
            ],
        ];
    }

    public function notifyWhenAdded(BreweryAddedEvent $event): void
    {
        $this->flashBag->add(
            'success',
            sprintf('%s was created successfully.', $event->getBrewery()->getName())
        );
    }

    public function notifyWhenEdited(BreweryEditedEvent $event): void
    {
        $this->flashBag->add(
            'success',
            sprintf('%s was edited succesfully.', $event->getBrewery()->getName())
        );
    }
}
