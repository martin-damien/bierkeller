<?php

namespace App\EventSubscriber;

use App\Event\BeerAddedToStockEvent;
use App\Event\BeerRemovalCanceledEvent;
use App\Event\StockEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

final class StockEventSubscriber implements EventSubscriberInterface
{
    private $flashBag;

    public function __construct(FlashBagInterface $flashBag)
    {
        $this->flashBag = $flashBag;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            StockEvents::BEER_ADDED => [
                ['notifyWhenBeerIsAdded', 0],
            ],
            StockEvents::BEER_REMOVAL_CANCELED => [
                ['notifyWhenBeerRemovalIsCanceled', 0]
            ]
        ];
    }

    public function notifyWhenBeerIsAdded(BeerAddedToStockEvent $event): void
    {
        $this->flashBag->add(
            'success',
            sprintf('%d units were added to %s.', $event->getQuantity(), $event->getBeer())
        );
    }

    public function notifyWhenBeerRemovalIsCanceled(BeerRemovalCanceledEvent $event): void
    {
        $this->flashBag->add(
            'error',
            sprintf('Stock has not been updated for "%s".', $event->getBeer())
        );
    }
}
