<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 */
class Beer
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var string The name of the beer
     *
     * @ORM\Column(type="text")
     * @ORM\OrderBy()
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $style;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Brewery")
     */
    private $brewery;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $alcohol;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $colour;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $bottleSize;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $bottleType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $notes;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Stock", mappedBy="beer")
     */
    private $stock;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->alcohol = 0.0;
        $this->rate = 0;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function getStyle(): ? string
    {
        return $this->style;
    }

    public function getBrewery(): ? Brewery
    {
        return $this->brewery;
    }

    public function getAlcohol(): float
    {
        return $this->alcohol;
    }

    public function getColour(): ? string
    {
        return $this->colour;
    }

    public function getBottleSize(): ? string
    {
        return $this->bottleSize;
    }

    public function getBottleType(): ? string
    {
        return $this->bottleType;
    }

    public function getNotes(): ? string
    {
        return $this->notes;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setRate(int $rate = null): void
    {
        if (null === $rate) {
            return;
        }

        $this->rate = $rate;
    }

    public function setStyle(string $style = null): void
    {
        $this->style = $style;
    }

    public function setBrewery(Brewery $brewery = null): void
    {
        $this->brewery = $brewery;
    }

    public function setAlcohol(float $alcohol = null): void
    {
        if (null === $alcohol) {
            return;
        }

        $this->alcohol = $alcohol;
    }

    public function setColour(string $colour = null): void
    {
        $this->colour = $colour;
    }

    public function setBottleSize(string $bottleSize = null): void
    {
        $this->bottleSize = $bottleSize;
    }

    public function setBottleType(string $bottleType = null): void
    {
        $this->bottleType = $bottleType;
    }

    public function setNotes(string $notes = null): void
    {
        $this->notes = $notes;
    }

    public function setStock(Stock $stock): void
    {
        $this->stock = $stock;
    }

    public function getStock():? Stock
    {
        return $this->stock;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
