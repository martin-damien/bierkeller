<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 */
class Stock
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Beer", inversedBy="stock")
     */
    protected $beer;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    public function __construct(Beer $beer)
    {
        $this->beer = $beer;
        $this->quantity = 0;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getBeer(): Beer
    {
        return $this->beer;
    }

    public function setBeer($beer): void
    {
        $this->beer = $beer;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }
}