<?php

namespace App\Handler;

use App\Command\AddBeerCommand;
use App\Command\AddStockCommand;
use App\Entity\Beer;
use App\Entity\Stock;
use App\Event\BeerAddedEvent;
use App\Event\BeerAddedToStockEvent;
use App\Event\BeerEvents;
use App\Event\StockEvents;
use App\Repository\BeerRepository;
use App\Repository\StockRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddStockCommandHandler implements MessageHandlerInterface
{
    private $eventDispatcher;
    private $beerRepository;
    private $stockRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        BeerRepository $beerRepository,
        StockRepository $stockRepository
    )
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->beerRepository = $beerRepository;
        $this->stockRepository = $stockRepository;
    }

    public function __invoke(AddStockCommand $command): Stock
    {
        /** @var Beer $beer */
        $beer = $command->beer;

        $stock = new Stock($beer);
        $stock->setQuantity($command->quantity);

        $beer->setStock($stock);

        $this->stockRepository->save($stock);
        $this->beerRepository->save($beer);

        $this->eventDispatcher->dispatch(
            StockEvents::BEER_ADDED,
            new BeerAddedToStockEvent($command->beer, $command->quantity)
        );

        return $stock;
    }
}
