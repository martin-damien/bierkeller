<?php

namespace App\Handler;

use App\Command\AddBreweryCommand;
use App\Entity\Brewery;
use App\Event\BreweryAddedEvent;
use App\Event\BreweryEvents;
use App\Repository\BreweryRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddBreweryCommandHandler implements MessageHandlerInterface
{
    private $eventDispatcher;
    private $breweryRepository;

    public function __construct(EventDispatcherInterface $eventDispatcher, BreweryRepository $breweryRepository)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->breweryRepository = $breweryRepository;
    }

    public function __invoke(AddBreweryCommand $command)
    {
        $brewery = new Brewery($command->name);
        $brewery->setCountry($command->country);

        $this->breweryRepository->save($brewery);

        $this->eventDispatcher->dispatch(BreweryEvents::ADDED, new BreweryAddedEvent($brewery));
    }
}
