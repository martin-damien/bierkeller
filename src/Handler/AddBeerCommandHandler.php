<?php

namespace App\Handler;

use App\Command\AddBeerCommand;
use App\Entity\Beer;
use App\Event\BeerAddedEvent;
use App\Event\BeerEvents;
use App\Repository\BeerRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddBeerCommandHandler implements MessageHandlerInterface
{
    private $eventDispatcher;
    private $beerRepository;

    public function __construct(EventDispatcherInterface $eventDispatcher, BeerRepository $beerRepository)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->beerRepository = $beerRepository;
    }

    public function __invoke(AddBeerCommand $command)
    {
        $beer = new Beer($command->name);
        $beer->setRate($command->rate);
        $beer->setStyle($command->style);
        $beer->setBrewery($command->brewery);
        $beer->setAlcohol($command->alcohol);
        $beer->setColour($command->colour);
        $beer->setBottleSize($command->bottleSize);
        $beer->setBottleType($command->bottleType);
        $beer->setNotes($command->notes);

        $this->beerRepository->save($beer);

        $this->eventDispatcher->dispatch(BeerEvents::ADDED, new BeerAddedEvent($beer));
    }
}
