<?php

namespace App\Handler;

use App\Command\EditBreweryCommand;
use App\Event\BreweryEditedEvent;
use App\Event\BreweryEvents;
use App\Repository\BreweryRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EditBreweryCommandHandler implements MessageHandlerInterface
{
    private $eventDispatcher;
    private $breweryRepository;

    public function __construct(EventDispatcherInterface $eventDispatcher, BreweryRepository $breweryRepository)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->breweryRepository = $breweryRepository;
    }

    public function __invoke(EditBreweryCommand $command)
    {
        $brewery = $command->getBrewery();
        $brewery->setName($command->name);
        $brewery->setCountry($command->country);

        $this->breweryRepository->save($brewery);

        $this->eventDispatcher->dispatch(BreweryEvents::EDITED, new BreweryEditedEvent($brewery));
    }
}
