<?php

namespace App\Handler;

use App\Command\AddBeerCommand;
use App\Command\AddOneBeerCommand;
use App\Command\AddStockCommand;
use App\Entity\Beer;
use App\Entity\Stock;
use App\Event\BeerAddedEvent;
use App\Event\BeerAddedToStockEvent;
use App\Event\BeerEvents;
use App\Event\StockEvents;
use App\Repository\BeerRepository;
use App\Repository\StockRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class AddOneBeerCommandHandler implements MessageHandlerInterface
{
    private $messageBus;
    private $eventDispatcher;
    private $stockRepository;

    public function __construct(
        MessageBusInterface $messageBus,
        EventDispatcherInterface $eventDispatcher,
        StockRepository $stockRepository
    )
    {
        $this->messageBus = $messageBus;
        $this->eventDispatcher = $eventDispatcher;
        $this->stockRepository = $stockRepository;
    }

    public function __invoke(AddOneBeerCommand $command)
    {
        /** @var Stock $stock */
        $stock = $this->stockRepository->findOneBy(['beer' => $command->beer]);

        if (!$stock) {
            $addStockCommand = new AddStockCommand();
            $addStockCommand->beer = $command->beer;
            $addStockCommand->quantity = 0;

            $stock = $this->messageBus->dispatch($addStockCommand);
        }

        $stock->setQuantity($stock->getQuantity() + 1);

        $this->stockRepository->save($stock);

        $this->eventDispatcher->dispatch(
            StockEvents::BEER_ADDED,
            new BeerAddedToStockEvent($command->beer, 1)
        );
    }
}
